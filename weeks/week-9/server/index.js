const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const mongoose = require('mongoose')
mongoose.connect('mongodb://127.0.0.1:27017/tokensAuthentication')
.then(db => console.log('connected'))
.catch(err => console.log(err))

const {saveSession,getSession} = require('./Controllers/sessionController')

const app = express();
app.use(morgan('dev'));
app.use(cors());

const bodyParser = require("body-parser");
app.use(bodyParser.json());

const indexRoutes = require('./routes/index')
app.use('/', indexRoutes)


app.post('/user/session',function(req,res){
    try {
      console.log(req.body.username)
        if(req.body.username && req.body.password){
            if(req.body.username == 'admin' && req.body.password == '123'){
                const session = saveSession(req.body.username)
                if(!session){
                    res.status(422);
                    res.json({
                      error: 'There was an error saving the session'
                    });
                }
                res.status(201).json({session});
            }
        }else{
            res.status(422);
            res.json({
                error: 'Invalid username or passowrd'
            })
        }
    } catch (error) {
        console.log(error);
    }
   
})


app.use(function(req,res,next){
    if(req.headers['authorization']){
        const token = req.headers['authorization'].split(' ')[1];
        const session = getSession(token);
        session.then(function(session){
            if(session){
                next();
                return;
            }else{
                res.status(401);
                res.send({
                  error: "Unauthorized "
                });
            }
        })
        
    }else{
        res.status(401);
        res.send({
          error: "Unauthorized "
        });
    } 
    
})



app.listen(3000,()=>{
    console.log('Example app running on :3000');
})