const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const session = new Schema({
    token: String,
    user: String,
    expire : Date
});

module.exports = mongoose.model('session',session);