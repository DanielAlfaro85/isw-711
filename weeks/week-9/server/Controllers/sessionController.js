const Session = require('../Models/sessionModel');
const randToken = require('rand-token');

const saveSession = function(username){
    console.log(username);
    const token = randToken.generate(16);
    const session = new Session();
    session.token = token;
    session.user = username;
    session.expire = new Date();
    session.save(function(err){
        if(err){
            console.log('error while saving a session',err)
        }
        
    })
    return session;
    
}

const getSession = function(token){
    return Session.findOne({token});
}

module.exports = {saveSession,getSession}