
breweriest();
function breweriest() {
  const url = 'https://api.openbrewerydb.org/breweries'
  fetch(url)
    .then(response => response.json())
    .then(data => {
      breweries = data.slice(0, 10);
      breweries.forEach(element => {
        let card = document.getElementById('cards');
        card.innerHTML += `<div class="card text-white bg-dark mb-3" style="width: 18rem; display: flex; margin: auto; text-align: center;">
                            <div class="card-header">${element.name}</div>
                              <div class="card-body">
                                  <a href="#" onclick="brewerie(${element.id})" class="btn btn-success">Info</p>
                            </div>
                          </div>`;
      });

    });

}

function brewerie(id) {
  const url = `https://api.openbrewerydb.org/breweries/${id}`
  fetch(url)  
    .then(response => response.json())
    .then(data => {
        let element = document.getElementById('cards');
        element.innerHTML = `<div class="card text-white bg-dark mb-3" style="width: 18rem; display: flex; margin: auto; text-align: center;">
                            <div class="card-header">${data.name}</div>
                              <div class="card-body">
                              <p class="card-text">Country: ${data.country}</p>
                              <p class="card-text">State: ${data.state}</p>
                              <p class="card-text">City: ${data.city}</p>
                              <a href="${data.website_url}">Website</a>
                              <div class="card-footer">
                                  <a href="#" onclick="resetweb()" class="btn btn-success">Info</a>
                                  </div>
                            </div>
                          </div>`;

    });

}

